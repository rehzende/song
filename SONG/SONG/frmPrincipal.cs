using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace SONG
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void fornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Song.Visualizacao.Shared.frmFornecedorListagem _frmFornecedorListagem = new Song.Visualizacao.Shared.frmFornecedorListagem();
            _frmFornecedorListagem.Show();
            
        }

        private void cadastrosToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void solicitarDesligamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.formPesquisa _Pesquisa = new Song.Visualizacao.Atendimento.formPesquisa();
            _Pesquisa.ShowDialog();
        }

    private void setorToolStripMenuItem1_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemSetor _frmListagemSetor = new Song.Visualizacao.Materiais.frmListagemSetor();
      _frmListagemSetor.Show();
    }

    private void EstoqueToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

        private void AtendimentoToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.Frmmenutemporario2 _frmMenuTemporarioAtendimento = new Song.Visualizacao.Atendimento.Frmmenutemporario2();
            _frmMenuTemporarioAtendimento.Show();
        }

        private void MateriaisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Materiais.frmMenuTemporarioMateriais _frmMenuTemporarioMateriais = new Song.Visualizacao.Materiais.frmMenuTemporarioMateriais();
            _frmMenuTemporarioMateriais.Show();
        }

        private void financeiroToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMenuTemporarioFinanceiro _frmMenuTemporarioFinanceiro = new Song.Visualizacao.Financeiro.frmMenuTemporarioFinanceiro();
            _frmMenuTemporarioFinanceiro.Show();
        }

    private void fechamentoToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmFechamentoEstoqueMensal _frmFechamentoEstoqueMensal = new Song.Visualizacao.Materiais.frmFechamentoEstoqueMensal();
      _frmFechamentoEstoqueMensal.Show();
    }

    private void setorToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemProdutos _frmListagemProdutos = new Song.Visualizacao.Materiais.frmListagemProdutos();
      _frmListagemProdutos.Show();
    }

        private void menusTemporáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void atendimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.Frmmenutemporario2 _frmAtedimentos = new Song.Visualizacao.Atendimento.Frmmenutemporario2();
            _frmAtedimentos.Show();
        }

    private void entradaDeNotaFiscalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemNF _frmListagemNF = new Song.Visualizacao.Materiais.frmListagemNF();
      _frmListagemNF.Show();
    }

    private void movimentosDoEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemMovimentosEstoque _frmListagemMovimentosEstoque = new Song.Visualizacao.Materiais.frmListagemMovimentosEstoque();
      _frmListagemMovimentosEstoque.Show();
    }

    private void solicitaçãoDeCompraToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemSolicitacaoCompra _frmListagemSolicitacaoCompra = new Song.Visualizacao.Materiais.frmListagemSolicitacaoCompra();
      _frmListagemSolicitacaoCompra.Show();
    }

    private void requisiçãoDeMateriaisToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemRequisicao _frmListagemRequisicao = new Song.Visualizacao.Materiais.frmListagemRequisicao();
      _frmListagemRequisicao.Show();
    }

    private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemInventario _frmListagemInventario = new Song.Visualizacao.Materiais.frmListagemInventario();
      _frmListagemInventario.Show();
    }

        private void movimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaListagem _frmContaListagem = new Song.Visualizacao.Financeiro.frmContaListagem();
            _frmContaListagem.Show();
        }

        private void gerarRemessaPGTOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRemessa _frmRemessa = new Song.Visualizacao.Financeiro.frmRemessa();
            _frmRemessa.Show();
        }

        private void importarRetornoPGTOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRetorno _frmRetorno = new Song.Visualizacao.Financeiro.frmRetorno();
            _frmRetorno.Show();
        }

        private void movimentosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaListagem _frmContaListagem = new Song.Visualizacao.Financeiro.frmContaListagem();
            _frmContaListagem.Show();
        }

        private void gerarRemessaCobrançaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRemessa _frmRemessa = new Song.Visualizacao.Financeiro.frmRemessa();
            _frmRemessa.Show();
        }

        private void importarRemessaCobrançaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRetorno _frmRetorno = new Song.Visualizacao.Financeiro.frmRetorno();
            _frmRetorno.Show();
        }

        private void movimentosToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoListagem _frmMovimentoCaixaBancoListagem = new Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoListagem();
            _frmMovimentoCaixaBancoListagem.Show();
        }

        private void saldosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoSaldos _frmMovimentoCaixaBancoSaldos = new Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoSaldos();
            _frmMovimentoCaixaBancoSaldos.Show();
        }

        private void campanhasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaListagem _frmCampanhaListagem = new Song.Visualizacao.Financeiro.frmCampanhaListagem();
            _frmCampanhaListagem.Show();
        }

        private void parceriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmParceriaListagem _frmParceriaListagem = new Song.Visualizacao.Financeiro.frmParceriaListagem();
            _frmParceriaListagem.Show();
        }

        private void dadosBancários2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCaixaBancoListagem _frmCaixaBancoListagem = new Song.Visualizacao.Financeiro.frmCaixaBancoListagem();
            _frmCaixaBancoListagem.Show();
        }

        private void testeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

    private void contatoToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Shared.frmContatoSong _frmContatoSong = new Song.Visualizacao.Shared.frmContatoSong();
      _frmContatoSong.Show();
    }

    private void sairToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

        private void títulosPendentesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CP_TitulosPendentes.pdf");
        }

        private void títulosPagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CP_TitulosPagos.pdf");
        }

        private void títulosPendentesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CR_TitulosPendentes.pdf");
        }

        private void títulosRecebidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CR_TitulosRecebidos.pdf");
        }

        private void extratoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_Extrato.pdf");
        }

        private void saldosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_Saldos.pdf");
        }

        private void capitaçãoDeRecursosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CAPREC_Previsoes.pdf");
        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_FluxoCaixa.pdf");
        }

    private void relatóriosToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmRelatorios _frmRelatorios = new Song.Visualizacao.Materiais.frmRelatorios();
      _frmRelatorios.Show();
    }

        private void relatóriosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRelatoriosFinanceiro _frmRelatoriosFinanceiro = new Song.Visualizacao.Financeiro.frmRelatoriosFinanceiro();
            _frmRelatoriosFinanceiro.Show();
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Shared.frmBackup _frmBackup = new Song.Visualizacao.Shared.frmBackup();
            _frmBackup.Show();
        }

        private void restoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Shared.frmRestore _frmRestore = new Song.Visualizacao.Shared.frmRestore();
            _frmRestore.Show();
        }

        private void projetoCompartilharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmAtendimentoPjc _frmAtendimentoPjc = new Song.Visualizacao.Atendimento.frmAtendimentoPjc();
            _frmAtendimentoPjc.Show();
        }

        private void atendimentosDiariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmaddatendimentodiario _frmaddatendimentodiario = new Song.Visualizacao.Atendimento.frmaddatendimentodiario();
            _frmaddatendimentodiario.Show();
        }

        private void abrigamentosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmAbrigados _frmAbrigados = new Song.Visualizacao.Atendimento.frmAbrigados();
            _frmAbrigados.Show();
        }

        private void agendaMedicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmprontuariomedico _frmprontagenda = new Song.Visualizacao.Atendimento.frmprontuariomedico();
            _frmprontagenda.Show();
        }

        private void inserirAtendimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmInsereAtendimento _frmInsereAtendimento = new Song.Visualizacao.Atendimento.frmInsereAtendimento();
            _frmInsereAtendimento.Show();
        }

        private void solicitarMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmsolicitarmedicamentos _frmsolicitarmedicamentos = new Song.Visualizacao.Atendimento.frmsolicitarmedicamentos();
            _frmsolicitarmedicamentos.Show();
        }

        private void darBaixaMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmdarbaixamedicamento _frmdarbaixamedicamento = new Song.Visualizacao.Atendimento.frmdarbaixamedicamento();
            _frmdarbaixamedicamento.Show();
        }

        private void abrigamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmrelatoriospjc _frmrelatoriospjc = new Song.Visualizacao.Atendimento.frmrelatoriospjc();
            _frmrelatoriospjc.Show();
        }

        private void popBaixaRendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmListaPopBaixaRenda _frmcadpopbaixarenda = new Song.Visualizacao.Atendimento.frmListaPopBaixaRenda();
            _frmcadpopbaixarenda.Show();
        }

        private void popDeRuaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmListaPoprua _frmcadpoprua = new Song.Visualizacao.Atendimento.frmListaPoprua();
            _frmcadpoprua.Show();
        }

        private void entidadesCadastradasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmListaEntidades _frmcadentidade = new Song.Visualizacao.Atendimento.frmListaEntidades();
            _frmcadentidade.Show();
        }
    }
}
