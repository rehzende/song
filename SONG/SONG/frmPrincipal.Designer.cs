﻿namespace SONG
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.menuSong = new System.Windows.Forms.MenuStrip();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.geraisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atendimentoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.popBaixaRendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.popDeRuaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entidadesCadastradasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.setorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.financeiroToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dadosBancários2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.projetoCompartilharToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atendimentosDiariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abrigamentosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.atendimentosDiariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agendaMedicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirAtendimentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitarMedicamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.darBaixaMedicamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrigamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitarDesligamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrosToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.entradaDeNotaFiscalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosDoEstoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fechamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitaçãoDeCompraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.requisiçãoDeMateriaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financeiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contasAPagarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarRemessaPGTOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarRetornoPGTOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contasAReceberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarRemessaCobrançaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarRemessaCobrançaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caixaEBancoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.saldosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.capitaçãoDeRecursosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.campanhasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parceriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contatoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSong.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuSong
            // 
            this.menuSong.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem,
            this.atendimentoToolStripMenuItem,
            this.estoqueToolStripMenuItem,
            this.financeiroToolStripMenuItem,
            this.utilitáriosToolStripMenuItem,
            this.sobreToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.menuSong.Location = new System.Drawing.Point(0, 0);
            this.menuSong.Name = "menuSong";
            this.menuSong.Size = new System.Drawing.Size(824, 24);
            this.menuSong.TabIndex = 0;
            this.menuSong.Text = "menuSong";
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.geraisToolStripMenuItem,
            this.atendimentoToolStripMenuItem1,
            this.estoqueToolStripMenuItem1,
            this.financeiroToolStripMenuItem1});
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.cadastrosToolStripMenuItem.Text = "Cadastros";
            // 
            // geraisToolStripMenuItem
            // 
            this.geraisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem,
            this.fornecedoresToolStripMenuItem});
            this.geraisToolStripMenuItem.Name = "geraisToolStripMenuItem";
            this.geraisToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.geraisToolStripMenuItem.Text = "Gerais";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // fornecedoresToolStripMenuItem
            // 
            this.fornecedoresToolStripMenuItem.Name = "fornecedoresToolStripMenuItem";
            this.fornecedoresToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.fornecedoresToolStripMenuItem.Text = "Fornecedores";
            this.fornecedoresToolStripMenuItem.Click += new System.EventHandler(this.fornecedoresToolStripMenuItem_Click);
            // 
            // atendimentoToolStripMenuItem1
            // 
            this.atendimentoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.popBaixaRendaToolStripMenuItem,
            this.popDeRuaToolStripMenuItem,
            this.entidadesCadastradasToolStripMenuItem});
            this.atendimentoToolStripMenuItem1.Name = "atendimentoToolStripMenuItem1";
            this.atendimentoToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.atendimentoToolStripMenuItem1.Text = "Atendimento";
            // 
            // popBaixaRendaToolStripMenuItem
            // 
            this.popBaixaRendaToolStripMenuItem.Name = "popBaixaRendaToolStripMenuItem";
            this.popBaixaRendaToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.popBaixaRendaToolStripMenuItem.Text = "Pop Baixa Renda";
            this.popBaixaRendaToolStripMenuItem.Click += new System.EventHandler(this.popBaixaRendaToolStripMenuItem_Click);
            // 
            // popDeRuaToolStripMenuItem
            // 
            this.popDeRuaToolStripMenuItem.Name = "popDeRuaToolStripMenuItem";
            this.popDeRuaToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.popDeRuaToolStripMenuItem.Text = "Pop de Rua";
            this.popDeRuaToolStripMenuItem.Click += new System.EventHandler(this.popDeRuaToolStripMenuItem_Click);
            // 
            // entidadesCadastradasToolStripMenuItem
            // 
            this.entidadesCadastradasToolStripMenuItem.Name = "entidadesCadastradasToolStripMenuItem";
            this.entidadesCadastradasToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.entidadesCadastradasToolStripMenuItem.Text = "Entidades Cadastradas";
            this.entidadesCadastradasToolStripMenuItem.Click += new System.EventHandler(this.entidadesCadastradasToolStripMenuItem_Click);
            // 
            // estoqueToolStripMenuItem1
            // 
            this.estoqueToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setorToolStripMenuItem,
            this.setorToolStripMenuItem1});
            this.estoqueToolStripMenuItem1.Name = "estoqueToolStripMenuItem1";
            this.estoqueToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.estoqueToolStripMenuItem1.Text = "Materiais";
            // 
            // setorToolStripMenuItem
            // 
            this.setorToolStripMenuItem.Name = "setorToolStripMenuItem";
            this.setorToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.setorToolStripMenuItem.Text = "Produtos";
            this.setorToolStripMenuItem.Click += new System.EventHandler(this.setorToolStripMenuItem_Click);
            // 
            // setorToolStripMenuItem1
            // 
            this.setorToolStripMenuItem1.Name = "setorToolStripMenuItem1";
            this.setorToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.setorToolStripMenuItem1.Text = "Setor";
            this.setorToolStripMenuItem1.Click += new System.EventHandler(this.setorToolStripMenuItem1_Click);
            // 
            // financeiroToolStripMenuItem1
            // 
            this.financeiroToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dadosBancários2ToolStripMenuItem});
            this.financeiroToolStripMenuItem1.Name = "financeiroToolStripMenuItem1";
            this.financeiroToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.financeiroToolStripMenuItem1.Text = "Financeiro";
            // 
            // dadosBancários2ToolStripMenuItem
            // 
            this.dadosBancários2ToolStripMenuItem.Name = "dadosBancários2ToolStripMenuItem";
            this.dadosBancários2ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.dadosBancários2ToolStripMenuItem.Text = "Dados Bancários 2";
            this.dadosBancários2ToolStripMenuItem.Click += new System.EventHandler(this.dadosBancários2ToolStripMenuItem_Click);
            // 
            // atendimentoToolStripMenuItem
            // 
            this.atendimentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem1,
            this.atendimentosDiariosToolStripMenuItem,
            this.abrigamentosToolStripMenuItem,
            this.solicitarDesligamentoToolStripMenuItem});
            this.atendimentoToolStripMenuItem.Name = "atendimentoToolStripMenuItem";
            this.atendimentoToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.atendimentoToolStripMenuItem.Text = "Atendimento";
            // 
            // cadastrosToolStripMenuItem1
            // 
            this.cadastrosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projetoCompartilharToolStripMenuItem,
            this.atendimentosDiariosToolStripMenuItem1,
            this.abrigamentosToolStripMenuItem1});
            this.cadastrosToolStripMenuItem1.Name = "cadastrosToolStripMenuItem1";
            this.cadastrosToolStripMenuItem1.Size = new System.Drawing.Size(175, 22);
            this.cadastrosToolStripMenuItem1.Text = "Atendimento";
            this.cadastrosToolStripMenuItem1.Click += new System.EventHandler(this.cadastrosToolStripMenuItem1_Click);
            // 
            // projetoCompartilharToolStripMenuItem
            // 
            this.projetoCompartilharToolStripMenuItem.Name = "projetoCompartilharToolStripMenuItem";
            this.projetoCompartilharToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.projetoCompartilharToolStripMenuItem.Text = "Projeto Compartilhar";
            this.projetoCompartilharToolStripMenuItem.Click += new System.EventHandler(this.projetoCompartilharToolStripMenuItem_Click);
            // 
            // atendimentosDiariosToolStripMenuItem1
            // 
            this.atendimentosDiariosToolStripMenuItem1.Name = "atendimentosDiariosToolStripMenuItem1";
            this.atendimentosDiariosToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.atendimentosDiariosToolStripMenuItem1.Text = "Atendimentos Diarios";
            this.atendimentosDiariosToolStripMenuItem1.Click += new System.EventHandler(this.atendimentosDiariosToolStripMenuItem1_Click);
            // 
            // abrigamentosToolStripMenuItem1
            // 
            this.abrigamentosToolStripMenuItem1.Name = "abrigamentosToolStripMenuItem1";
            this.abrigamentosToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.abrigamentosToolStripMenuItem1.Text = "Abrigamentos";
            this.abrigamentosToolStripMenuItem1.Click += new System.EventHandler(this.abrigamentosToolStripMenuItem1_Click);
            // 
            // atendimentosDiariosToolStripMenuItem
            // 
            this.atendimentosDiariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agendaMedicaToolStripMenuItem,
            this.inserirAtendimentosToolStripMenuItem,
            this.solicitarMedicamentosToolStripMenuItem,
            this.darBaixaMedicamentosToolStripMenuItem});
            this.atendimentosDiariosToolStripMenuItem.Name = "atendimentosDiariosToolStripMenuItem";
            this.atendimentosDiariosToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.atendimentosDiariosToolStripMenuItem.Text = "Prontuario";
            // 
            // agendaMedicaToolStripMenuItem
            // 
            this.agendaMedicaToolStripMenuItem.Name = "agendaMedicaToolStripMenuItem";
            this.agendaMedicaToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.agendaMedicaToolStripMenuItem.Text = "Agenda medica";
            this.agendaMedicaToolStripMenuItem.Click += new System.EventHandler(this.agendaMedicaToolStripMenuItem_Click);
            // 
            // inserirAtendimentosToolStripMenuItem
            // 
            this.inserirAtendimentosToolStripMenuItem.Name = "inserirAtendimentosToolStripMenuItem";
            this.inserirAtendimentosToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.inserirAtendimentosToolStripMenuItem.Text = "Inserir Atendimentos";
            this.inserirAtendimentosToolStripMenuItem.Click += new System.EventHandler(this.inserirAtendimentosToolStripMenuItem_Click);
            // 
            // solicitarMedicamentosToolStripMenuItem
            // 
            this.solicitarMedicamentosToolStripMenuItem.Name = "solicitarMedicamentosToolStripMenuItem";
            this.solicitarMedicamentosToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.solicitarMedicamentosToolStripMenuItem.Text = "Solicitar  Medicamentos";
            this.solicitarMedicamentosToolStripMenuItem.Click += new System.EventHandler(this.solicitarMedicamentosToolStripMenuItem_Click);
            // 
            // darBaixaMedicamentosToolStripMenuItem
            // 
            this.darBaixaMedicamentosToolStripMenuItem.Name = "darBaixaMedicamentosToolStripMenuItem";
            this.darBaixaMedicamentosToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.darBaixaMedicamentosToolStripMenuItem.Text = "Dar Baixa Medicamentos";
            this.darBaixaMedicamentosToolStripMenuItem.Click += new System.EventHandler(this.darBaixaMedicamentosToolStripMenuItem_Click);
            // 
            // abrigamentosToolStripMenuItem
            // 
            this.abrigamentosToolStripMenuItem.Name = "abrigamentosToolStripMenuItem";
            this.abrigamentosToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.abrigamentosToolStripMenuItem.Text = "Relatórios";
            this.abrigamentosToolStripMenuItem.Click += new System.EventHandler(this.abrigamentosToolStripMenuItem_Click);
            // 
            // solicitarDesligamentoToolStripMenuItem
            // 
            this.solicitarDesligamentoToolStripMenuItem.Name = "solicitarDesligamentoToolStripMenuItem";
            this.solicitarDesligamentoToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.solicitarDesligamentoToolStripMenuItem.Text = "Consultar Cadastro";
            this.solicitarDesligamentoToolStripMenuItem.Click += new System.EventHandler(this.solicitarDesligamentoToolStripMenuItem_Click);
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem2,
            this.movimentaçãoToolStripMenuItem,
            this.comprasToolStripMenuItem,
            this.requisiçãoDeMateriaisToolStripMenuItem,
            this.relatóriosToolStripMenuItem});
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.estoqueToolStripMenuItem.Text = "Materiais";
            this.estoqueToolStripMenuItem.Click += new System.EventHandler(this.EstoqueToolStripMenuItem_Click);
            // 
            // cadastrosToolStripMenuItem2
            // 
            this.cadastrosToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.entradaDeNotaFiscalToolStripMenuItem});
            this.cadastrosToolStripMenuItem2.Name = "cadastrosToolStripMenuItem2";
            this.cadastrosToolStripMenuItem2.Size = new System.Drawing.Size(198, 22);
            this.cadastrosToolStripMenuItem2.Text = "Notas";
            // 
            // entradaDeNotaFiscalToolStripMenuItem
            // 
            this.entradaDeNotaFiscalToolStripMenuItem.Name = "entradaDeNotaFiscalToolStripMenuItem";
            this.entradaDeNotaFiscalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.entradaDeNotaFiscalToolStripMenuItem.Text = "Entrada de Nota Fiscal";
            this.entradaDeNotaFiscalToolStripMenuItem.Click += new System.EventHandler(this.entradaDeNotaFiscalToolStripMenuItem_Click);
            // 
            // movimentaçãoToolStripMenuItem
            // 
            this.movimentaçãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosDoEstoqueToolStripMenuItem,
            this.inventarioToolStripMenuItem,
            this.fechamentoToolStripMenuItem});
            this.movimentaçãoToolStripMenuItem.Name = "movimentaçãoToolStripMenuItem";
            this.movimentaçãoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.movimentaçãoToolStripMenuItem.Text = "Movimentação";
            // 
            // movimentosDoEstoqueToolStripMenuItem
            // 
            this.movimentosDoEstoqueToolStripMenuItem.Name = "movimentosDoEstoqueToolStripMenuItem";
            this.movimentosDoEstoqueToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.movimentosDoEstoqueToolStripMenuItem.Text = "Movimentos do Estoque";
            this.movimentosDoEstoqueToolStripMenuItem.Click += new System.EventHandler(this.movimentosDoEstoqueToolStripMenuItem_Click);
            // 
            // inventarioToolStripMenuItem
            // 
            this.inventarioToolStripMenuItem.Name = "inventarioToolStripMenuItem";
            this.inventarioToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.inventarioToolStripMenuItem.Text = "Inventario";
            this.inventarioToolStripMenuItem.Click += new System.EventHandler(this.inventarioToolStripMenuItem_Click);
            // 
            // fechamentoToolStripMenuItem
            // 
            this.fechamentoToolStripMenuItem.Name = "fechamentoToolStripMenuItem";
            this.fechamentoToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.fechamentoToolStripMenuItem.Text = "Fechamento Mensal do Estoque";
            this.fechamentoToolStripMenuItem.Click += new System.EventHandler(this.fechamentoToolStripMenuItem_Click);
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.solicitaçãoDeCompraToolStripMenuItem});
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.comprasToolStripMenuItem.Text = "Compras";
            // 
            // solicitaçãoDeCompraToolStripMenuItem
            // 
            this.solicitaçãoDeCompraToolStripMenuItem.Name = "solicitaçãoDeCompraToolStripMenuItem";
            this.solicitaçãoDeCompraToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.solicitaçãoDeCompraToolStripMenuItem.Text = "Solicitação de Compra";
            this.solicitaçãoDeCompraToolStripMenuItem.Click += new System.EventHandler(this.solicitaçãoDeCompraToolStripMenuItem_Click);
            // 
            // requisiçãoDeMateriaisToolStripMenuItem
            // 
            this.requisiçãoDeMateriaisToolStripMenuItem.Name = "requisiçãoDeMateriaisToolStripMenuItem";
            this.requisiçãoDeMateriaisToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.requisiçãoDeMateriaisToolStripMenuItem.Text = "Requisição de Materiais";
            this.requisiçãoDeMateriaisToolStripMenuItem.Click += new System.EventHandler(this.requisiçãoDeMateriaisToolStripMenuItem_Click);
            // 
            // relatóriosToolStripMenuItem
            // 
            this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
            this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.relatóriosToolStripMenuItem.Text = "Relatórios";
            this.relatóriosToolStripMenuItem.Click += new System.EventHandler(this.relatóriosToolStripMenuItem_Click);
            // 
            // financeiroToolStripMenuItem
            // 
            this.financeiroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contasAPagarToolStripMenuItem,
            this.contasAReceberToolStripMenuItem,
            this.caixaEBancoToolStripMenuItem,
            this.capitaçãoDeRecursosToolStripMenuItem,
            this.relatóriosToolStripMenuItem1});
            this.financeiroToolStripMenuItem.Name = "financeiroToolStripMenuItem";
            this.financeiroToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.financeiroToolStripMenuItem.Text = "Financeiro";
            // 
            // contasAPagarToolStripMenuItem
            // 
            this.contasAPagarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosToolStripMenuItem,
            this.gerarRemessaPGTOToolStripMenuItem,
            this.importarRetornoPGTOToolStripMenuItem});
            this.contasAPagarToolStripMenuItem.Name = "contasAPagarToolStripMenuItem";
            this.contasAPagarToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.contasAPagarToolStripMenuItem.Text = "Contas a Pagar";
            // 
            // movimentosToolStripMenuItem
            // 
            this.movimentosToolStripMenuItem.Name = "movimentosToolStripMenuItem";
            this.movimentosToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.movimentosToolStripMenuItem.Text = "Movimentos";
            this.movimentosToolStripMenuItem.Click += new System.EventHandler(this.movimentosToolStripMenuItem_Click);
            // 
            // gerarRemessaPGTOToolStripMenuItem
            // 
            this.gerarRemessaPGTOToolStripMenuItem.Name = "gerarRemessaPGTOToolStripMenuItem";
            this.gerarRemessaPGTOToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.gerarRemessaPGTOToolStripMenuItem.Text = "Gerar Remessa PGTO";
            this.gerarRemessaPGTOToolStripMenuItem.Click += new System.EventHandler(this.gerarRemessaPGTOToolStripMenuItem_Click);
            // 
            // importarRetornoPGTOToolStripMenuItem
            // 
            this.importarRetornoPGTOToolStripMenuItem.Name = "importarRetornoPGTOToolStripMenuItem";
            this.importarRetornoPGTOToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.importarRetornoPGTOToolStripMenuItem.Text = "Importar Retorno PGTO";
            this.importarRetornoPGTOToolStripMenuItem.Click += new System.EventHandler(this.importarRetornoPGTOToolStripMenuItem_Click);
            // 
            // contasAReceberToolStripMenuItem
            // 
            this.contasAReceberToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosToolStripMenuItem1,
            this.gerarRemessaCobrançaToolStripMenuItem,
            this.importarRemessaCobrançaToolStripMenuItem});
            this.contasAReceberToolStripMenuItem.Name = "contasAReceberToolStripMenuItem";
            this.contasAReceberToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.contasAReceberToolStripMenuItem.Text = "Contas a Receber";
            // 
            // movimentosToolStripMenuItem1
            // 
            this.movimentosToolStripMenuItem1.Name = "movimentosToolStripMenuItem1";
            this.movimentosToolStripMenuItem1.Size = new System.Drawing.Size(223, 22);
            this.movimentosToolStripMenuItem1.Text = "Movimentos";
            this.movimentosToolStripMenuItem1.Click += new System.EventHandler(this.movimentosToolStripMenuItem1_Click);
            // 
            // gerarRemessaCobrançaToolStripMenuItem
            // 
            this.gerarRemessaCobrançaToolStripMenuItem.Name = "gerarRemessaCobrançaToolStripMenuItem";
            this.gerarRemessaCobrançaToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.gerarRemessaCobrançaToolStripMenuItem.Text = "Gerar Remessa Cobrança";
            this.gerarRemessaCobrançaToolStripMenuItem.Click += new System.EventHandler(this.gerarRemessaCobrançaToolStripMenuItem_Click);
            // 
            // importarRemessaCobrançaToolStripMenuItem
            // 
            this.importarRemessaCobrançaToolStripMenuItem.Name = "importarRemessaCobrançaToolStripMenuItem";
            this.importarRemessaCobrançaToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.importarRemessaCobrançaToolStripMenuItem.Text = "Importar Remessa Cobrança";
            this.importarRemessaCobrançaToolStripMenuItem.Click += new System.EventHandler(this.importarRemessaCobrançaToolStripMenuItem_Click);
            // 
            // caixaEBancoToolStripMenuItem
            // 
            this.caixaEBancoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosToolStripMenuItem2,
            this.saldosToolStripMenuItem});
            this.caixaEBancoToolStripMenuItem.Name = "caixaEBancoToolStripMenuItem";
            this.caixaEBancoToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.caixaEBancoToolStripMenuItem.Text = "Caixa e Banco";
            // 
            // movimentosToolStripMenuItem2
            // 
            this.movimentosToolStripMenuItem2.Name = "movimentosToolStripMenuItem2";
            this.movimentosToolStripMenuItem2.Size = new System.Drawing.Size(141, 22);
            this.movimentosToolStripMenuItem2.Text = "Movimentos";
            this.movimentosToolStripMenuItem2.Click += new System.EventHandler(this.movimentosToolStripMenuItem2_Click);
            // 
            // saldosToolStripMenuItem
            // 
            this.saldosToolStripMenuItem.Name = "saldosToolStripMenuItem";
            this.saldosToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.saldosToolStripMenuItem.Text = "Saldos";
            this.saldosToolStripMenuItem.Click += new System.EventHandler(this.saldosToolStripMenuItem_Click);
            // 
            // capitaçãoDeRecursosToolStripMenuItem
            // 
            this.capitaçãoDeRecursosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.campanhasToolStripMenuItem,
            this.parceriasToolStripMenuItem});
            this.capitaçãoDeRecursosToolStripMenuItem.Name = "capitaçãoDeRecursosToolStripMenuItem";
            this.capitaçãoDeRecursosToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.capitaçãoDeRecursosToolStripMenuItem.Text = "Capitação de Recursos";
            // 
            // campanhasToolStripMenuItem
            // 
            this.campanhasToolStripMenuItem.Name = "campanhasToolStripMenuItem";
            this.campanhasToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.campanhasToolStripMenuItem.Text = "Campanhas";
            this.campanhasToolStripMenuItem.Click += new System.EventHandler(this.campanhasToolStripMenuItem_Click);
            // 
            // parceriasToolStripMenuItem
            // 
            this.parceriasToolStripMenuItem.Name = "parceriasToolStripMenuItem";
            this.parceriasToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.parceriasToolStripMenuItem.Text = "Parcerias";
            this.parceriasToolStripMenuItem.Click += new System.EventHandler(this.parceriasToolStripMenuItem_Click);
            // 
            // relatóriosToolStripMenuItem1
            // 
            this.relatóriosToolStripMenuItem1.Name = "relatóriosToolStripMenuItem1";
            this.relatóriosToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.relatóriosToolStripMenuItem1.Text = "Relatórios";
            this.relatóriosToolStripMenuItem1.Click += new System.EventHandler(this.relatóriosToolStripMenuItem1_Click);
            // 
            // utilitáriosToolStripMenuItem
            // 
            this.utilitáriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem,
            this.restoreToolStripMenuItem});
            this.utilitáriosToolStripMenuItem.Name = "utilitáriosToolStripMenuItem";
            this.utilitáriosToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.utilitáriosToolStripMenuItem.Text = "Utilitários";
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.backupToolStripMenuItem.Text = "Backup";
            this.backupToolStripMenuItem.Click += new System.EventHandler(this.backupToolStripMenuItem_Click);
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            this.restoreToolStripMenuItem.Click += new System.EventHandler(this.restoreToolStripMenuItem_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contatoToolStripMenuItem});
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.sobreToolStripMenuItem.Text = "Sobre";
            // 
            // contatoToolStripMenuItem
            // 
            this.contatoToolStripMenuItem.Name = "contatoToolStripMenuItem";
            this.contatoToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.contatoToolStripMenuItem.Text = "Contato";
            this.contatoToolStripMenuItem.Click += new System.EventHandler(this.contatoToolStripMenuItem_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 374);
            this.Controls.Add(this.menuSong);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuSong;
            this.Name = "frmPrincipal";
            this.Text = "Sistema SONG";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.menuSong.ResumeLayout(false);
            this.menuSong.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuSong;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financeiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem geraisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fornecedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atendimentoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem financeiroToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem popBaixaRendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem popDeRuaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entidadesCadastradasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projetoCompartilharToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atendimentosDiariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abrigamentosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem atendimentosDiariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agendaMedicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirAtendimentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitarMedicamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem darBaixaMedicamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrigamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitarDesligamentoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem setorToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem setorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dadosBancários2ToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem entradaDeNotaFiscalToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem movimentaçãoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem movimentosDoEstoqueToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem fechamentoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem solicitaçãoDeCompraToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem requisiçãoDeMateriaisToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contasAPagarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contasAReceberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caixaEBancoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem capitaçãoDeRecursosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerarRemessaPGTOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importarRetornoPGTOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerarRemessaCobrançaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarRemessaCobrançaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentosToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem saldosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem campanhasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parceriasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilitáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem contatoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem1;
    }
}

