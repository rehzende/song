using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Shared
{
    public partial class frmFornecedorListagem : Form
    {
        public frmFornecedorListagem()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmFornecedorListagem_Load(object sender, EventArgs e)
        {

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Shared.frmFornecedorCadastro _frmFornecedorCadastro = new frmFornecedorCadastro();
            _frmFornecedorCadastro.Show();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Shared.frmFornecedorCadastro _frmFornecedorCadastro = new frmFornecedorCadastro();
            _frmFornecedorCadastro.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Shared.frmFornecedorCadastro _frmFornecedorCadastro = new frmFornecedorCadastro();
            _frmFornecedorCadastro.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Shared.frmFornecedorCadastro _frmFornecedorCadastro = new frmFornecedorCadastro();
            _frmFornecedorCadastro.Show();
        }
    }
}
