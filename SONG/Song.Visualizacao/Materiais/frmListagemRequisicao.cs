using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Materiais
{
  public partial class frmListagemRequisicao : Form
  {
    public frmListagemRequisicao()
    {
      InitializeComponent();
    }

    private void btnVisualizar_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmCadastroRequisicao _frmCadastroRequisicao = new Song.Visualizacao.Materiais.frmCadastroRequisicao();
      _frmCadastroRequisicao.Show();
    }

    private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void btnIncluir_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmCadastroRequisicao _frmCadastroRequisicao = new Song.Visualizacao.Materiais.frmCadastroRequisicao();
      _frmCadastroRequisicao.Show();
    }

    private void btnAlterar_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmCadastroRequisicao _frmCadastroRequisicao = new Song.Visualizacao.Materiais.frmCadastroRequisicao();
      _frmCadastroRequisicao.Show();
    }

    private void btnExcluir_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmCadastroRequisicao _frmCadastroRequisicao = new Song.Visualizacao.Materiais.frmCadastroRequisicao();
      _frmCadastroRequisicao.Show();
    }

    private void btnVoltar_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
