﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Materiais
{
  public partial class frmListagemSetor : Form
  {
    public frmListagemSetor()
    {
      InitializeComponent();
    }

    private void FrmCadastroSetor_Load(object sender, EventArgs e)
    {

    }

    private void btnIncluir_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.FrmCadastroSetor _frmCadastroSetor = new Song.Visualizacao.Materiais.FrmCadastroSetor();
      _frmCadastroSetor.Show();
    }

    private void btnAlterar_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.FrmCadastroSetor _frmCadastroSetor = new Song.Visualizacao.Materiais.FrmCadastroSetor();
      _frmCadastroSetor.Show();
    }

    private void btnExcluir_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.FrmCadastroSetor _frmCadastroSetor = new Song.Visualizacao.Materiais.FrmCadastroSetor();
      _frmCadastroSetor.Show();
    }

    private void btnVisualizar_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.FrmCadastroSetor _frmCadastroSetor = new Song.Visualizacao.Materiais.FrmCadastroSetor();
      _frmCadastroSetor.Show();
    }

    private void btnVoltar_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
