﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Materiais
{
  public partial class frmListagemMovimentosEstoque : Form
  {
    public frmListagemMovimentosEstoque()
    {
      InitializeComponent();
    }

    private void btnIncluir_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmMovimentosEstoque _frmMovimentosEstoque = new Song.Visualizacao.Materiais.frmMovimentosEstoque();
      _frmMovimentosEstoque.Show();
    }

    private void btnAlterar_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmMovimentosEstoque _frmMovimentosEstoque = new Song.Visualizacao.Materiais.frmMovimentosEstoque();
      _frmMovimentosEstoque.Show();
    }

    private void btnExcluir_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmMovimentosEstoque _frmMovimentosEstoque = new Song.Visualizacao.Materiais.frmMovimentosEstoque();
      _frmMovimentosEstoque.Show();
    }

    private void btnVisualizar_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmMovimentosEstoque _frmMovimentosEstoque = new Song.Visualizacao.Materiais.frmMovimentosEstoque();
      _frmMovimentosEstoque.Show();
    }

    private void btnVoltar_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
