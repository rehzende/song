using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Materiais
{
    public partial class frmMenuTemporarioMateriais : Form
    {
        public frmMenuTemporarioMateriais()
        {
            InitializeComponent();
        }

    private void sairToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void setorToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemProdutos _frmListagemProdutos = new Song.Visualizacao.Materiais.frmListagemProdutos();
      _frmListagemProdutos.Show();
    }

    private void setorToolStripMenuItem1_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemSetor _frmListagemSetor = new Song.Visualizacao.Materiais.frmListagemSetor();
      _frmListagemSetor.Show();
    }

    private void frmMenuTemporarioMateriais_Load(object sender, EventArgs e)
    {

    }

    private void entradaDeNotaFiscalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemNF _frmListagemNF = new Song.Visualizacao.Materiais.frmListagemNF();
      _frmListagemNF.Show();
    }

    private void movimentosDoEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemMovimentosEstoque _frmListagemMovimentosEstoque = new Song.Visualizacao.Materiais.frmListagemMovimentosEstoque();
      _frmListagemMovimentosEstoque.Show();
    }

    private void fechamentoToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmFechamentoEstoqueMensal _frmFechamentoEstoqueMensal = new Song.Visualizacao.Materiais.frmFechamentoEstoqueMensal();
      _frmFechamentoEstoqueMensal.Show();
    }

    private void solicitaçãoDeCompraToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemSolicitacaoCompra _frmListagemSolicitacaoCompra = new Song.Visualizacao.Materiais.frmListagemSolicitacaoCompra();
      _frmListagemSolicitacaoCompra.Show();
    }

    private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmProcessarInventario _frmProcessarInventario = new Song.Visualizacao.Materiais.frmProcessarInventario();
      _frmProcessarInventario.Show();
    }

    private void requisiçãoDeMateriaisToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Song.Visualizacao.Materiais.frmListagemRequisicao _frmListagemRequisicao = new Song.Visualizacao.Materiais.frmListagemRequisicao();
      _frmListagemRequisicao.Show();
    }

    private void situaToolStripMenuItem_Click(object sender, EventArgs e)
    {
 
    }

    private void entradasSaídasToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    private void estoqueMaxMínToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    private void toolStripMenuItem5_Click(object sender, EventArgs e)
    {

    }
  }
}
