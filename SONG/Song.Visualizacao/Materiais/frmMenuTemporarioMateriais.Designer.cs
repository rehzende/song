﻿namespace Song.Visualizacao.Materiais
{
    partial class frmMenuTemporarioMateriais
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.setorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrosToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.entradaDeNotaFiscalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosDoEstoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fechamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitaçãoDeCompraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.requisiçãoDeMateriaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.situaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entradasSaídasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueMaxMínToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroToolStripMenuItem,
            this.toolStripMenuItem1,
            this.sairToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(939, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.estoqueToolStripMenuItem1});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            // 
            // estoqueToolStripMenuItem1
            // 
            this.estoqueToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setorToolStripMenuItem,
            this.setorToolStripMenuItem1});
            this.estoqueToolStripMenuItem1.Name = "estoqueToolStripMenuItem1";
            this.estoqueToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.estoqueToolStripMenuItem1.Text = "Estoque";
            // 
            // setorToolStripMenuItem
            // 
            this.setorToolStripMenuItem.Name = "setorToolStripMenuItem";
            this.setorToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.setorToolStripMenuItem.Text = "Produtos";
            this.setorToolStripMenuItem.Click += new System.EventHandler(this.setorToolStripMenuItem_Click);
            // 
            // setorToolStripMenuItem1
            // 
            this.setorToolStripMenuItem1.Name = "setorToolStripMenuItem1";
            this.setorToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.setorToolStripMenuItem1.Text = "Setor";
            this.setorToolStripMenuItem1.Click += new System.EventHandler(this.setorToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem2,
            this.movimentaçãoToolStripMenuItem,
            this.comprasToolStripMenuItem,
            this.requisiçãoDeMateriaisToolStripMenuItem,
            this.relatóriosToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(67, 20);
            this.toolStripMenuItem1.Text = "Materiais";
            // 
            // cadastrosToolStripMenuItem2
            // 
            this.cadastrosToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.entradaDeNotaFiscalToolStripMenuItem});
            this.cadastrosToolStripMenuItem2.Name = "cadastrosToolStripMenuItem2";
            this.cadastrosToolStripMenuItem2.Size = new System.Drawing.Size(198, 22);
            this.cadastrosToolStripMenuItem2.Text = "Notas";
            // 
            // entradaDeNotaFiscalToolStripMenuItem
            // 
            this.entradaDeNotaFiscalToolStripMenuItem.Name = "entradaDeNotaFiscalToolStripMenuItem";
            this.entradaDeNotaFiscalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.entradaDeNotaFiscalToolStripMenuItem.Text = "Entrada de Nota Fiscal";
            this.entradaDeNotaFiscalToolStripMenuItem.Click += new System.EventHandler(this.entradaDeNotaFiscalToolStripMenuItem_Click);
            // 
            // movimentaçãoToolStripMenuItem
            // 
            this.movimentaçãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosDoEstoqueToolStripMenuItem,
            this.inventarioToolStripMenuItem,
            this.fechamentoToolStripMenuItem});
            this.movimentaçãoToolStripMenuItem.Name = "movimentaçãoToolStripMenuItem";
            this.movimentaçãoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.movimentaçãoToolStripMenuItem.Text = "Movimentação";
            // 
            // movimentosDoEstoqueToolStripMenuItem
            // 
            this.movimentosDoEstoqueToolStripMenuItem.Name = "movimentosDoEstoqueToolStripMenuItem";
            this.movimentosDoEstoqueToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.movimentosDoEstoqueToolStripMenuItem.Text = "Movimentos do Estoque";
            this.movimentosDoEstoqueToolStripMenuItem.Click += new System.EventHandler(this.movimentosDoEstoqueToolStripMenuItem_Click);
            // 
            // inventarioToolStripMenuItem
            // 
            this.inventarioToolStripMenuItem.Name = "inventarioToolStripMenuItem";
            this.inventarioToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.inventarioToolStripMenuItem.Text = "Inventario";
            this.inventarioToolStripMenuItem.Click += new System.EventHandler(this.inventarioToolStripMenuItem_Click);
            // 
            // fechamentoToolStripMenuItem
            // 
            this.fechamentoToolStripMenuItem.Name = "fechamentoToolStripMenuItem";
            this.fechamentoToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.fechamentoToolStripMenuItem.Text = "Fechamento Mensal do Estoque";
            this.fechamentoToolStripMenuItem.Click += new System.EventHandler(this.fechamentoToolStripMenuItem_Click);
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.solicitaçãoDeCompraToolStripMenuItem});
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.comprasToolStripMenuItem.Text = "Compras";
            // 
            // solicitaçãoDeCompraToolStripMenuItem
            // 
            this.solicitaçãoDeCompraToolStripMenuItem.Name = "solicitaçãoDeCompraToolStripMenuItem";
            this.solicitaçãoDeCompraToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.solicitaçãoDeCompraToolStripMenuItem.Text = "Solicitação de Compra";
            this.solicitaçãoDeCompraToolStripMenuItem.Click += new System.EventHandler(this.solicitaçãoDeCompraToolStripMenuItem_Click);
            // 
            // requisiçãoDeMateriaisToolStripMenuItem
            // 
            this.requisiçãoDeMateriaisToolStripMenuItem.Name = "requisiçãoDeMateriaisToolStripMenuItem";
            this.requisiçãoDeMateriaisToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.requisiçãoDeMateriaisToolStripMenuItem.Text = "Requisição de Materiais";
            this.requisiçãoDeMateriaisToolStripMenuItem.Click += new System.EventHandler(this.requisiçãoDeMateriaisToolStripMenuItem_Click);
            // 
            // relatóriosToolStripMenuItem
            // 
            this.relatóriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.situaToolStripMenuItem,
            this.entradasSaídasToolStripMenuItem,
            this.estoqueMaxMínToolStripMenuItem,
            this.toolStripMenuItem2});
            this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
            this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.relatóriosToolStripMenuItem.Text = "Relatórios";
            // 
            // situaToolStripMenuItem
            // 
            this.situaToolStripMenuItem.Name = "situaToolStripMenuItem";
            this.situaToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.situaToolStripMenuItem.Text = "Situação do Estoque";
            this.situaToolStripMenuItem.Click += new System.EventHandler(this.situaToolStripMenuItem_Click);
            // 
            // entradasSaídasToolStripMenuItem
            // 
            this.entradasSaídasToolStripMenuItem.Name = "entradasSaídasToolStripMenuItem";
            this.entradasSaídasToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.entradasSaídasToolStripMenuItem.Text = "Entradas/Saídas";
            this.entradasSaídasToolStripMenuItem.Click += new System.EventHandler(this.entradasSaídasToolStripMenuItem_Click);
            // 
            // estoqueMaxMínToolStripMenuItem
            // 
            this.estoqueMaxMínToolStripMenuItem.Name = "estoqueMaxMínToolStripMenuItem";
            this.estoqueMaxMínToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.estoqueMaxMínToolStripMenuItem.Text = "Estoque Max/Mín";
            this.estoqueMaxMínToolStripMenuItem.Click += new System.EventHandler(this.estoqueMaxMínToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(181, 22);
            this.toolStripMenuItem2.Text = "Relatório/Compras";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem5.Text = "Solicitações Realizadas";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // frmMenuTemporarioMateriais
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 329);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenuTemporarioMateriais";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenuTemporarioMateriais";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMenuTemporarioMateriais_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem entradaDeNotaFiscalToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem movimentaçãoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem movimentosDoEstoqueToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem fechamentoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem solicitaçãoDeCompraToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem requisiçãoDeMateriaisToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem situaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem entradasSaídasToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem estoqueMaxMínToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
    private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem setorToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem setorToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
  }
}