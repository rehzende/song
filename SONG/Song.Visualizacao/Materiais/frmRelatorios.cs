using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Materiais
{
  public partial class frmRelatorios : Form
  {
    public frmRelatorios()
    {
      InitializeComponent();
    }

    private void btnVisualizar_Click(object sender, EventArgs e)
    {
      string Texto = combocat.SelectedItem.ToString();



      if (Texto == "Entradas e Saídas do Estoque")
      {
        System.Diagnostics.Process.Start(@"C:\Relatorios\ES_EntradasSaidas.pdf");
      }
      else if (Texto == "Maxímo e Minímo")
      {
        System.Diagnostics.Process.Start(@"C:\Relatorios\ES_MaxMin.pdf");
      }
      else if (Texto == "Situação do Estoque")
      {
        System.Diagnostics.Process.Start(@"C:\Relatorios\ES_SituacaoEstoque.pdf");
      }
      else if (Texto == "Solicitação de Compra")
      {
        System.Diagnostics.Process.Start(@"C:\Relatorios\ES_SolicitacaoCompra.pdf");
      }
      else if (Texto == "Produtos em Estoque Minímo")
      {
        System.Diagnostics.Process.Start(@"C:\Relatorios\ES_ProdutoComEstoqueMin.pdf");
      }
        }

    private void btnVoltar_Click(object sender, EventArgs e)
    {
      this.Close();
    }

        private void combocat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
