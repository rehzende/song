﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmMenuTemporarioFinanceiro : Form
    {
        public frmMenuTemporarioFinanceiro()
        {
            InitializeComponent();
        }

        private void dadosBancáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dadosBancáriosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCaixaBancoListagem _frmCaixaBancoListagem = new frmCaixaBancoListagem();
            _frmCaixaBancoListagem.Show();
        }

        private void movimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaListagem _frmContaListagem = new frmContaListagem();
            _frmContaListagem.Show();
        }

        private void movimentosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaListagem _frmContaListagem = new frmContaListagem();
            _frmContaListagem.Show();
        }

        private void gerarRemessaPGTOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRemessa _frmRemessa = new frmRemessa();
            _frmRemessa.Show();
        }

        private void importarRetornoPGTOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRetorno _frmRetorno = new frmRetorno();
            _frmRetorno.Show();
        }

        private void gerarRemessaCobrançaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRemessa _frmRemessa = new frmRemessa();
            _frmRemessa.Show();
        }

        private void importarRemessaCobrançaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRetorno _frmRetorno = new frmRetorno();
            _frmRetorno.Show();
        }

        private void movimentosToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoListagem _frmMovimentoCaixaBancoListagem = new frmMovimentoCaixaBancoListagem();
            _frmMovimentoCaixaBancoListagem.Show();
        }

        private void sadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoSaldos _frmMovimentoCaixaBancoSaldos = new frmMovimentoCaixaBancoSaldos();
            _frmMovimentoCaixaBancoSaldos.Show();
        }

        private void campanhasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaListagem _frmCampanhaListagem = new frmCampanhaListagem();
            _frmCampanhaListagem.Show();
        }

        private void parceriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmParceriaListagem _frmParceriaListagem = new frmParceriaListagem();
            _frmParceriaListagem.Show();
        }

        private void títulosVencidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CP_TitulosPendentes.pdf");
        }

        private void títulosPagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CP_TitulosPagos.pdf");
        }

        private void títulosVencidosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CR_TitulosPendentes.pdf");
        }

        private void títulosRecebidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CR_TitulosRecebidos.pdf");
        }

        private void extratoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_Extrato.pdf");
        }

        private void saldosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_Saldos.pdf");
        }

        private void previsõesPorPeríodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CAPREC_Previsoes.pdf");
        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_FluxoCaixa.pdf");
        }

        private void relatoriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRelatorios _frmRelatorio = new Song.Visualizacao.Financeiro.frmRelatorios();
            _frmRelatorio.Show();
        }

        private void relatoriosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmRelatoriosFinanceiro _frmRelatoriosFinanceiro = new frmRelatoriosFinanceiro();
            _frmRelatoriosFinanceiro.Show();
        }
    }
}
