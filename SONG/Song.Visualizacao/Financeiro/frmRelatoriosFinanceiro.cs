﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmRelatoriosFinanceiro : Form
    {
        public frmRelatoriosFinanceiro()
        {
            InitializeComponent();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            string Texto = combocat.SelectedItem.ToString();

            
            if (Texto == "Contas a Pagar - Titulos Pendentes")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CP_TitulosPendentes.pdf");
            }
            else if (Texto == "Contas a Pagar - Titulos Pagos")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CP_TitulosPagos.pdf");
            }
            else if (Texto == "Contas a Receber - Titulos Pendentes")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CR_TitulosPendentes.pdf");
            }
            else if (Texto == "Contas a Receber - Titulos Recebidos")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CR_TitulosRecebidos.pdf");
            }
            else if (Texto == "Caixa e Banco - Extrato")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_Extrato.pdf");
            }
            else if (Texto == "Caixa e Banco - Saldos")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_Saldos.pdf");
            }
            else if (Texto == "Capitação de Recursos - Previsões")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CAPREC_Previsoes.pdf");
            }
            else if (Texto == "Fluxo de Caixa")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\CXBC_FluxoCaixa.pdf");
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
