﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmRelatorios : Form
    {
        public frmRelatorios()
        {
            InitializeComponent();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {


            string Texto = subcombo.SelectedItem.ToString();

            if (Texto == "Projeto Compartilhar")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\PJC.pdf");
            }
            else if (Texto == "Abrigados")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\pessoasabrigadas.pdf");
            }
            else if (Texto == "Atendimentos")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\diario.pdf");
            }
            else
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\Parceira.pdf");
            }


        }

        private void combocat_MouseCaptureChanged(object sender, EventArgs e)
        {

            string submenu = combocatfinan.SelectedItem.ToString();

            if (submenu == "Contas a Pagar")
            {
                subcombo.Items.Add("Titulos Pendentes");
                subcombo.Items.Add("Titulos a pagar");
                
            }
            else if (submenu == "Contas a Receber")
            {
                subcombo.Items.Add("Titulos Pendentes");
                subcombo.Items.Add("Titulos Recebidos");
            }
            else if (submenu == "Caixa e Banco")
            {
                subcombo.Items.Add("Extrato");
                subcombo.Items.Add("Saldos");
            }
            else
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\Parceira.pdf");
            }
        }
    }
}
