﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmBaixasListagem : Form
    {
        public frmBaixasListagem()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmBaixasCadastro _frmBaixasCadastro = new frmBaixasCadastro();
            _frmBaixasCadastro.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmBaixasCadastro _frmBaixasCadastro = new frmBaixasCadastro();
            _frmBaixasCadastro.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmBaixasCadastro _frmBaixasCadastro = new frmBaixasCadastro();
            _frmBaixasCadastro.Show();
        }
    }
}
