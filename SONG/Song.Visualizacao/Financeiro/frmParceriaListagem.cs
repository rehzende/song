﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmParceriaListagem : Form
    {
        public frmParceriaListagem()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmParceriaProcessar _frmParceriaProcessar = new frmParceriaProcessar();
            _frmParceriaProcessar.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
