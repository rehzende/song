﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmCaixaBancoListagem : Form
    {
        public frmCaixaBancoListagem()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCaixaBancoCadastro _frmCaixaBancoCadastro = new frmCaixaBancoCadastro();
            _frmCaixaBancoCadastro.Show();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCaixaBancoCadastro _frmCaixaBancoCadastro = new frmCaixaBancoCadastro();
            _frmCaixaBancoCadastro.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCaixaBancoCadastro _frmCaixaBancoCadastro = new frmCaixaBancoCadastro();
            _frmCaixaBancoCadastro.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCaixaBancoCadastro _frmCaixaBancoCadastro = new frmCaixaBancoCadastro();
            _frmCaixaBancoCadastro.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
