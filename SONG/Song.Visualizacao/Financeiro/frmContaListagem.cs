﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmContaListagem : Form
    {
        public frmContaListagem()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaCadastro _frmContaCadastro = new frmContaCadastro();
            _frmContaCadastro.Show();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaCadastro _frmContaCadastro = new frmContaCadastro();
            _frmContaCadastro.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaCadastro _frmContaCadastro = new frmContaCadastro();
            _frmContaCadastro.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmContaCadastro _frmContaCadastro = new frmContaCadastro();
            _frmContaCadastro.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmBaixasListagem _frmBaixasListagem = new frmBaixasListagem();
            _frmBaixasListagem.Show();
        }
    }
}
