﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmMovimentoCaixaBancoListagem : Form
    {
        public frmMovimentoCaixaBancoListagem()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoCadastro _frmMovimentoCaixaBancoCadastro = new frmMovimentoCaixaBancoCadastro();
            _frmMovimentoCaixaBancoCadastro.Show();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoCadastro _frmMovimentoCaixaBancoCadastro = new frmMovimentoCaixaBancoCadastro();
            _frmMovimentoCaixaBancoCadastro.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoCadastro _frmMovimentoCaixaBancoCadastro = new frmMovimentoCaixaBancoCadastro();
            _frmMovimentoCaixaBancoCadastro.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoCadastro _frmMovimentoCaixaBancoCadastro = new frmMovimentoCaixaBancoCadastro();
            _frmMovimentoCaixaBancoCadastro.Show();
        }

        private void btnTransferencia_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmMovimentoCaixaBancoTransferencia _frmMovimentoCaixaBancoTransferencia = new frmMovimentoCaixaBancoTransferencia();
            _frmMovimentoCaixaBancoTransferencia.Show();
        }
    }
}
