﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Financeiro
{
    public partial class frmCampanhaListagem : Form
    {
        public frmCampanhaListagem()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaCadastro _frmCampanhaCadastro = new frmCampanhaCadastro();
            _frmCampanhaCadastro.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaCadastro _frmCampanhaCadastro = new frmCampanhaCadastro();
            _frmCampanhaCadastro.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaCadastro _frmCampanhaCadastro = new frmCampanhaCadastro();
            _frmCampanhaCadastro.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaCadastro _frmCampanhaCadastro = new frmCampanhaCadastro();
            _frmCampanhaCadastro.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaContatos _frmCampanhaContatos = new frmCampanhaContatos();
            _frmCampanhaContatos.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Financeiro.frmCampanhaFechamento _frmCampanhaFechamento = new frmCampanhaFechamento();
            _frmCampanhaFechamento.Show();
        }
    }
}
