﻿namespace Song.Visualizacao.Financeiro
{
    partial class frmMenuTemporarioFinanceiro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dadosBancáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dadosBancáriosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contasAPagarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarRemessaPGTOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarRetornoPGTOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contasAReceberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarRemessaCobrançaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarRemessaCobrançaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caixaEBancoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentosToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.sadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.capitaçãoDeRecursosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.campanhasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parceriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contasAPagarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.títulosVencidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.títulosPagosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contasAReceberToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.títulosVencidosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.títulosRecebidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caixaEBancoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.extratoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saldosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.capitaçãoDeRecursosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.previsõesPorPeríodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatoriosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroToolStripMenuItem,
            this.relatoriosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(928, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dadosBancáriosToolStripMenuItem,
            this.contasAPagarToolStripMenuItem,
            this.contasAReceberToolStripMenuItem,
            this.caixaEBancoToolStripMenuItem,
            this.capitaçãoDeRecursosToolStripMenuItem,
            this.relatóriosToolStripMenuItem});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.cadastroToolStripMenuItem.Text = "Financeiro";
            this.cadastroToolStripMenuItem.Click += new System.EventHandler(this.cadastroToolStripMenuItem_Click);
            // 
            // dadosBancáriosToolStripMenuItem
            // 
            this.dadosBancáriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dadosBancáriosToolStripMenuItem1});
            this.dadosBancáriosToolStripMenuItem.Name = "dadosBancáriosToolStripMenuItem";
            this.dadosBancáriosToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.dadosBancáriosToolStripMenuItem.Text = "Cadastro";
            this.dadosBancáriosToolStripMenuItem.Click += new System.EventHandler(this.dadosBancáriosToolStripMenuItem_Click);
            // 
            // dadosBancáriosToolStripMenuItem1
            // 
            this.dadosBancáriosToolStripMenuItem1.Name = "dadosBancáriosToolStripMenuItem1";
            this.dadosBancáriosToolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
            this.dadosBancáriosToolStripMenuItem1.Text = "Dados Bancários";
            this.dadosBancáriosToolStripMenuItem1.Click += new System.EventHandler(this.dadosBancáriosToolStripMenuItem1_Click);
            // 
            // contasAPagarToolStripMenuItem
            // 
            this.contasAPagarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosToolStripMenuItem,
            this.gerarRemessaPGTOToolStripMenuItem,
            this.importarRetornoPGTOToolStripMenuItem});
            this.contasAPagarToolStripMenuItem.Name = "contasAPagarToolStripMenuItem";
            this.contasAPagarToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.contasAPagarToolStripMenuItem.Text = "Contas a Pagar";
            // 
            // movimentosToolStripMenuItem
            // 
            this.movimentosToolStripMenuItem.Name = "movimentosToolStripMenuItem";
            this.movimentosToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.movimentosToolStripMenuItem.Text = "Movimentos";
            this.movimentosToolStripMenuItem.Click += new System.EventHandler(this.movimentosToolStripMenuItem_Click);
            // 
            // gerarRemessaPGTOToolStripMenuItem
            // 
            this.gerarRemessaPGTOToolStripMenuItem.Name = "gerarRemessaPGTOToolStripMenuItem";
            this.gerarRemessaPGTOToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.gerarRemessaPGTOToolStripMenuItem.Text = "Gerar Remessa PGTO";
            this.gerarRemessaPGTOToolStripMenuItem.Click += new System.EventHandler(this.gerarRemessaPGTOToolStripMenuItem_Click);
            // 
            // importarRetornoPGTOToolStripMenuItem
            // 
            this.importarRetornoPGTOToolStripMenuItem.Name = "importarRetornoPGTOToolStripMenuItem";
            this.importarRetornoPGTOToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.importarRetornoPGTOToolStripMenuItem.Text = "Importar Retorno PGTO";
            this.importarRetornoPGTOToolStripMenuItem.Click += new System.EventHandler(this.importarRetornoPGTOToolStripMenuItem_Click);
            // 
            // contasAReceberToolStripMenuItem
            // 
            this.contasAReceberToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosToolStripMenuItem1,
            this.gerarRemessaCobrançaToolStripMenuItem,
            this.importarRemessaCobrançaToolStripMenuItem});
            this.contasAReceberToolStripMenuItem.Name = "contasAReceberToolStripMenuItem";
            this.contasAReceberToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.contasAReceberToolStripMenuItem.Text = "Contas a Receber";
            // 
            // movimentosToolStripMenuItem1
            // 
            this.movimentosToolStripMenuItem1.Name = "movimentosToolStripMenuItem1";
            this.movimentosToolStripMenuItem1.Size = new System.Drawing.Size(223, 22);
            this.movimentosToolStripMenuItem1.Text = "Movimentos";
            this.movimentosToolStripMenuItem1.Click += new System.EventHandler(this.movimentosToolStripMenuItem1_Click);
            // 
            // gerarRemessaCobrançaToolStripMenuItem
            // 
            this.gerarRemessaCobrançaToolStripMenuItem.Name = "gerarRemessaCobrançaToolStripMenuItem";
            this.gerarRemessaCobrançaToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.gerarRemessaCobrançaToolStripMenuItem.Text = "Gerar Remessa Cobrança";
            this.gerarRemessaCobrançaToolStripMenuItem.Click += new System.EventHandler(this.gerarRemessaCobrançaToolStripMenuItem_Click);
            // 
            // importarRemessaCobrançaToolStripMenuItem
            // 
            this.importarRemessaCobrançaToolStripMenuItem.Name = "importarRemessaCobrançaToolStripMenuItem";
            this.importarRemessaCobrançaToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.importarRemessaCobrançaToolStripMenuItem.Text = "Importar Remessa Cobrança";
            this.importarRemessaCobrançaToolStripMenuItem.Click += new System.EventHandler(this.importarRemessaCobrançaToolStripMenuItem_Click);
            // 
            // caixaEBancoToolStripMenuItem
            // 
            this.caixaEBancoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.movimentosToolStripMenuItem2,
            this.sadosToolStripMenuItem});
            this.caixaEBancoToolStripMenuItem.Name = "caixaEBancoToolStripMenuItem";
            this.caixaEBancoToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.caixaEBancoToolStripMenuItem.Text = "Caixa e Banco";
            // 
            // movimentosToolStripMenuItem2
            // 
            this.movimentosToolStripMenuItem2.Name = "movimentosToolStripMenuItem2";
            this.movimentosToolStripMenuItem2.Size = new System.Drawing.Size(141, 22);
            this.movimentosToolStripMenuItem2.Text = "Movimentos";
            this.movimentosToolStripMenuItem2.Click += new System.EventHandler(this.movimentosToolStripMenuItem2_Click);
            // 
            // sadosToolStripMenuItem
            // 
            this.sadosToolStripMenuItem.Name = "sadosToolStripMenuItem";
            this.sadosToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.sadosToolStripMenuItem.Text = "Saldos";
            this.sadosToolStripMenuItem.Click += new System.EventHandler(this.sadosToolStripMenuItem_Click);
            // 
            // capitaçãoDeRecursosToolStripMenuItem
            // 
            this.capitaçãoDeRecursosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.campanhasToolStripMenuItem,
            this.parceriasToolStripMenuItem});
            this.capitaçãoDeRecursosToolStripMenuItem.Name = "capitaçãoDeRecursosToolStripMenuItem";
            this.capitaçãoDeRecursosToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.capitaçãoDeRecursosToolStripMenuItem.Text = "Capitação de Recursos";
            // 
            // campanhasToolStripMenuItem
            // 
            this.campanhasToolStripMenuItem.Name = "campanhasToolStripMenuItem";
            this.campanhasToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.campanhasToolStripMenuItem.Text = "Campanhas";
            this.campanhasToolStripMenuItem.Click += new System.EventHandler(this.campanhasToolStripMenuItem_Click);
            // 
            // parceriasToolStripMenuItem
            // 
            this.parceriasToolStripMenuItem.Name = "parceriasToolStripMenuItem";
            this.parceriasToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.parceriasToolStripMenuItem.Text = "Parcerias";
            this.parceriasToolStripMenuItem.Click += new System.EventHandler(this.parceriasToolStripMenuItem_Click);
            // 
            // relatóriosToolStripMenuItem
            // 
            this.relatóriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contasAPagarToolStripMenuItem1,
            this.contasAReceberToolStripMenuItem1,
            this.caixaEBancoToolStripMenuItem1,
            this.capitaçãoDeRecursosToolStripMenuItem1,
            this.fluxoDeCaixaToolStripMenuItem,
            this.relatoriosToolStripMenuItem1});
            this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
            this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.relatóriosToolStripMenuItem.Text = "Relatórios";
            // 
            // contasAPagarToolStripMenuItem1
            // 
            this.contasAPagarToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.títulosVencidosToolStripMenuItem,
            this.títulosPagosToolStripMenuItem});
            this.contasAPagarToolStripMenuItem1.Name = "contasAPagarToolStripMenuItem1";
            this.contasAPagarToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.contasAPagarToolStripMenuItem1.Text = "Contas a Pagar";
            // 
            // títulosVencidosToolStripMenuItem
            // 
            this.títulosVencidosToolStripMenuItem.Name = "títulosVencidosToolStripMenuItem";
            this.títulosVencidosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.títulosVencidosToolStripMenuItem.Text = "Títulos Pendentes";
            this.títulosVencidosToolStripMenuItem.Click += new System.EventHandler(this.títulosVencidosToolStripMenuItem_Click);
            // 
            // títulosPagosToolStripMenuItem
            // 
            this.títulosPagosToolStripMenuItem.Name = "títulosPagosToolStripMenuItem";
            this.títulosPagosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.títulosPagosToolStripMenuItem.Text = "Títulos Pagos";
            this.títulosPagosToolStripMenuItem.Click += new System.EventHandler(this.títulosPagosToolStripMenuItem_Click);
            // 
            // contasAReceberToolStripMenuItem1
            // 
            this.contasAReceberToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.títulosVencidosToolStripMenuItem1,
            this.títulosRecebidosToolStripMenuItem});
            this.contasAReceberToolStripMenuItem1.Name = "contasAReceberToolStripMenuItem1";
            this.contasAReceberToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.contasAReceberToolStripMenuItem1.Text = "Contas a Receber";
            // 
            // títulosVencidosToolStripMenuItem1
            // 
            this.títulosVencidosToolStripMenuItem1.Name = "títulosVencidosToolStripMenuItem1";
            this.títulosVencidosToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.títulosVencidosToolStripMenuItem1.Text = "Títulos Pendentes";
            this.títulosVencidosToolStripMenuItem1.Click += new System.EventHandler(this.títulosVencidosToolStripMenuItem1_Click);
            // 
            // títulosRecebidosToolStripMenuItem
            // 
            this.títulosRecebidosToolStripMenuItem.Name = "títulosRecebidosToolStripMenuItem";
            this.títulosRecebidosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.títulosRecebidosToolStripMenuItem.Text = "Títulos Recebidos";
            this.títulosRecebidosToolStripMenuItem.Click += new System.EventHandler(this.títulosRecebidosToolStripMenuItem_Click);
            // 
            // caixaEBancoToolStripMenuItem1
            // 
            this.caixaEBancoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.extratoToolStripMenuItem,
            this.saldosToolStripMenuItem});
            this.caixaEBancoToolStripMenuItem1.Name = "caixaEBancoToolStripMenuItem1";
            this.caixaEBancoToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.caixaEBancoToolStripMenuItem1.Text = "Caixa e Banco";
            // 
            // extratoToolStripMenuItem
            // 
            this.extratoToolStripMenuItem.Name = "extratoToolStripMenuItem";
            this.extratoToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.extratoToolStripMenuItem.Text = "Extrato";
            this.extratoToolStripMenuItem.Click += new System.EventHandler(this.extratoToolStripMenuItem_Click);
            // 
            // saldosToolStripMenuItem
            // 
            this.saldosToolStripMenuItem.Name = "saldosToolStripMenuItem";
            this.saldosToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.saldosToolStripMenuItem.Text = "Saldos";
            this.saldosToolStripMenuItem.Click += new System.EventHandler(this.saldosToolStripMenuItem_Click);
            // 
            // capitaçãoDeRecursosToolStripMenuItem1
            // 
            this.capitaçãoDeRecursosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.previsõesPorPeríodoToolStripMenuItem});
            this.capitaçãoDeRecursosToolStripMenuItem1.Name = "capitaçãoDeRecursosToolStripMenuItem1";
            this.capitaçãoDeRecursosToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.capitaçãoDeRecursosToolStripMenuItem1.Text = "Capitação de Recursos";
            // 
            // previsõesPorPeríodoToolStripMenuItem
            // 
            this.previsõesPorPeríodoToolStripMenuItem.Name = "previsõesPorPeríodoToolStripMenuItem";
            this.previsõesPorPeríodoToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.previsõesPorPeríodoToolStripMenuItem.Text = "Previsões por Período";
            this.previsõesPorPeríodoToolStripMenuItem.Click += new System.EventHandler(this.previsõesPorPeríodoToolStripMenuItem_Click);
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Fluxo de Caixa";
            this.fluxoDeCaixaToolStripMenuItem.Click += new System.EventHandler(this.fluxoDeCaixaToolStripMenuItem_Click);
            // 
            // relatoriosToolStripMenuItem
            // 
            this.relatoriosToolStripMenuItem.Name = "relatoriosToolStripMenuItem";
            this.relatoriosToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.relatoriosToolStripMenuItem.Text = "Relatorios";
            this.relatoriosToolStripMenuItem.Click += new System.EventHandler(this.relatoriosToolStripMenuItem_Click);
            // 
            // relatoriosToolStripMenuItem1
            // 
            this.relatoriosToolStripMenuItem1.Name = "relatoriosToolStripMenuItem1";
            this.relatoriosToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.relatoriosToolStripMenuItem1.Text = "Relatorios";
            this.relatoriosToolStripMenuItem1.Click += new System.EventHandler(this.relatoriosToolStripMenuItem1_Click);
            // 
            // frmMenuTemporarioFinanceiro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 318);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenuTemporarioFinanceiro";
            this.Text = "frmMenuTemporario";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dadosBancáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dadosBancáriosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem contasAPagarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerarRemessaPGTOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarRetornoPGTOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contasAReceberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem gerarRemessaCobrançaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarRemessaCobrançaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caixaEBancoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentosToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem capitaçãoDeRecursosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem campanhasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parceriasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contasAPagarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem títulosVencidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem títulosPagosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contasAReceberToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem títulosVencidosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem títulosRecebidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caixaEBancoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem capitaçãoDeRecursosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extratoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saldosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previsõesPorPeríodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatoriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatoriosToolStripMenuItem1;
    }
}