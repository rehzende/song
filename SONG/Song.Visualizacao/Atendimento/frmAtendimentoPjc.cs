﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Atendimento
{
    public partial class frmAtendimentoPjc : Form
    {
        public frmAtendimentoPjc()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmadicionapjc _frmchamatdpjc = new Song.Visualizacao.Atendimento.frmadicionapjc();
            _frmchamatdpjc.Show();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmmudardiapjc _frmAlteraDia = new Song.Visualizacao.Atendimento.frmmudardiapjc();
                _frmAlteraDia.Show();
        }
    }
}
