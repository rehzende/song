﻿namespace Song.Visualizacao.Atendimento
{
    partial class Frmmenutemporario2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.popBaixaRendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.popDeRuaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.agendaMedicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirAtendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitarMedicamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.darBaixaMedicamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.projetoCompartilharToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atendimentoDiarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrigamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitarDesligamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarCadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(777, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.relatóriosToolStripMenuItem,
            this.consultarCadastroToolStripMenuItem});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            this.cadastroToolStripMenuItem.Click += new System.EventHandler(this.cadastroToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.popBaixaRendaToolStripMenuItem,
            this.popDeRuaToolStripMenuItem,
            this.entidadesToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(175, 22);
            this.toolStripMenuItem2.Text = "Cadastro";
            // 
            // popBaixaRendaToolStripMenuItem
            // 
            this.popBaixaRendaToolStripMenuItem.Name = "popBaixaRendaToolStripMenuItem";
            this.popBaixaRendaToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.popBaixaRendaToolStripMenuItem.Text = "Pop Baixa Renda";
            this.popBaixaRendaToolStripMenuItem.Click += new System.EventHandler(this.popBaixaRendaToolStripMenuItem_Click);
            // 
            // popDeRuaToolStripMenuItem
            // 
            this.popDeRuaToolStripMenuItem.Name = "popDeRuaToolStripMenuItem";
            this.popDeRuaToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.popDeRuaToolStripMenuItem.Text = "Pop de Rua";
            this.popDeRuaToolStripMenuItem.Click += new System.EventHandler(this.popDeRuaToolStripMenuItem_Click);
            // 
            // entidadesToolStripMenuItem
            // 
            this.entidadesToolStripMenuItem.Name = "entidadesToolStripMenuItem";
            this.entidadesToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.entidadesToolStripMenuItem.Text = "Entidades";
            this.entidadesToolStripMenuItem.Click += new System.EventHandler(this.entidadesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agendaMedicaToolStripMenuItem,
            this.inserirAtendimentoToolStripMenuItem,
            this.solicitarMedicamentosToolStripMenuItem,
            this.darBaixaMedicamentosToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(175, 22);
            this.toolStripMenuItem3.Text = "Prontuário";
            // 
            // agendaMedicaToolStripMenuItem
            // 
            this.agendaMedicaToolStripMenuItem.Name = "agendaMedicaToolStripMenuItem";
            this.agendaMedicaToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.agendaMedicaToolStripMenuItem.Text = "Agenda Medica";
            this.agendaMedicaToolStripMenuItem.Click += new System.EventHandler(this.agendaMedicaToolStripMenuItem_Click);
            // 
            // inserirAtendimentoToolStripMenuItem
            // 
            this.inserirAtendimentoToolStripMenuItem.Name = "inserirAtendimentoToolStripMenuItem";
            this.inserirAtendimentoToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.inserirAtendimentoToolStripMenuItem.Text = "Inserir Atendimento";
            this.inserirAtendimentoToolStripMenuItem.Click += new System.EventHandler(this.inserirAtendimentoToolStripMenuItem_Click);
            // 
            // solicitarMedicamentosToolStripMenuItem
            // 
            this.solicitarMedicamentosToolStripMenuItem.Name = "solicitarMedicamentosToolStripMenuItem";
            this.solicitarMedicamentosToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.solicitarMedicamentosToolStripMenuItem.Text = "Solicitar Medicamentos";
            this.solicitarMedicamentosToolStripMenuItem.Click += new System.EventHandler(this.solicitarMedicamentosToolStripMenuItem_Click);
            // 
            // darBaixaMedicamentosToolStripMenuItem
            // 
            this.darBaixaMedicamentosToolStripMenuItem.Name = "darBaixaMedicamentosToolStripMenuItem";
            this.darBaixaMedicamentosToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.darBaixaMedicamentosToolStripMenuItem.Text = "Dar Baixa Medicamentos";
            this.darBaixaMedicamentosToolStripMenuItem.Click += new System.EventHandler(this.darBaixaMedicamentosToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projetoCompartilharToolStripMenuItem,
            this.atendimentoDiarioToolStripMenuItem,
            this.abrigamentoToolStripMenuItem,
            this.solicitarDesligamentoToolStripMenuItem});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(175, 22);
            this.toolStripMenuItem4.Text = "Atendimentos";
            // 
            // projetoCompartilharToolStripMenuItem
            // 
            this.projetoCompartilharToolStripMenuItem.Name = "projetoCompartilharToolStripMenuItem";
            this.projetoCompartilharToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.projetoCompartilharToolStripMenuItem.Text = "Projeto Compartilhar";
            this.projetoCompartilharToolStripMenuItem.Click += new System.EventHandler(this.projetoCompartilharToolStripMenuItem_Click);
            // 
            // atendimentoDiarioToolStripMenuItem
            // 
            this.atendimentoDiarioToolStripMenuItem.Name = "atendimentoDiarioToolStripMenuItem";
            this.atendimentoDiarioToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.atendimentoDiarioToolStripMenuItem.Text = "Atendimento Diario";
            this.atendimentoDiarioToolStripMenuItem.Click += new System.EventHandler(this.atendimentoDiarioToolStripMenuItem_Click);
            // 
            // abrigamentoToolStripMenuItem
            // 
            this.abrigamentoToolStripMenuItem.Name = "abrigamentoToolStripMenuItem";
            this.abrigamentoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.abrigamentoToolStripMenuItem.Text = "Abrigamento";
            this.abrigamentoToolStripMenuItem.Click += new System.EventHandler(this.abrigamentoToolStripMenuItem_Click);
            // 
            // solicitarDesligamentoToolStripMenuItem
            // 
            this.solicitarDesligamentoToolStripMenuItem.Name = "solicitarDesligamentoToolStripMenuItem";
            this.solicitarDesligamentoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.solicitarDesligamentoToolStripMenuItem.Text = "Solicitar Desligamento";
            // 
            // relatóriosToolStripMenuItem
            // 
            this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
            this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.relatóriosToolStripMenuItem.Text = "Relatórios";
            this.relatóriosToolStripMenuItem.Click += new System.EventHandler(this.relatóriosToolStripMenuItem_Click);
            // 
            // consultarCadastroToolStripMenuItem
            // 
            this.consultarCadastroToolStripMenuItem.Name = "consultarCadastroToolStripMenuItem";
            this.consultarCadastroToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.consultarCadastroToolStripMenuItem.Text = "Consultar Cadastro";
            this.consultarCadastroToolStripMenuItem.Click += new System.EventHandler(this.consultarCadastroToolStripMenuItem_Click);
            // 
            // Frmmenutemporario2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 261);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Frmmenutemporario2";
            this.Text = "Frmmenutemporario2";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem popBaixaRendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem popDeRuaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem agendaMedicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirAtendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarCadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitarMedicamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem darBaixaMedicamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projetoCompartilharToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atendimentoDiarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrigamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitarDesligamentoToolStripMenuItem;
    }
}