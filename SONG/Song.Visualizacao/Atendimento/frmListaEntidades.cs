﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Atendimento
{
    public partial class frmListaEntidades : Form
    {
        public frmListaEntidades()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmcadentidade _frmcadentidade = new Song.Visualizacao.Atendimento.frmcadentidade();
            _frmcadentidade.Show();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmcadentidade _frmcadentidade = new Song.Visualizacao.Atendimento.frmcadentidade();
            _frmcadentidade.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmcadentidade _frmcadentidade = new Song.Visualizacao.Atendimento.frmcadentidade();
            _frmcadentidade.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmcadentidade _frmcadentidade = new Song.Visualizacao.Atendimento.frmcadentidade();
            _frmcadentidade.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
