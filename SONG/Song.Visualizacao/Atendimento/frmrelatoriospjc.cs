﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Atendimento
{
    public partial class frmrelatoriospjc : Form
    {
        public frmrelatoriospjc()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"Song.Visualizacao.Primeiro Andar.pdf");
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {


            string Texto = combocat.SelectedItem.ToString();
           


            if (Texto == "Projeto Compartilhar")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\PJC.pdf");
            }
            else if (Texto == "Abrigados")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\pessoasabrigadas.pdf");
            }
            else if (Texto == "Atendimentos")
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\diario.pdf");
            }
            else
            {
                System.Diagnostics.Process.Start(@"C:\Relatorios\Parceira.pdf");
            }

            
        }

        private bool ToString(string v)
        {
            throw new NotImplementedException();
        }
    }
}
