﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Atendimento
{
    public partial class frmprontuariomedico : Form
    {
        public frmprontuariomedico()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmprontcadagenda _frmInsereAgenda = new Song.Visualizacao.Atendimento.frmprontcadagenda();
            _frmInsereAgenda.Show();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmprontcadagenda _frmInsereAgenda = new Song.Visualizacao.Atendimento.frmprontcadagenda();
            _frmInsereAgenda.Show();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmprontcadagenda _frmInsereAgenda = new Song.Visualizacao.Atendimento.frmprontcadagenda();
            _frmInsereAgenda.Show();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmprontcadagenda _frmInsereAgenda = new Song.Visualizacao.Atendimento.frmprontcadagenda();
            _frmInsereAgenda.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmprontcadagenda _frmInsereAgenda = new Song.Visualizacao.Atendimento.frmprontcadagenda();
            _frmInsereAgenda.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
