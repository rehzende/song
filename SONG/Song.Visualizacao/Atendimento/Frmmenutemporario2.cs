﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Song.Visualizacao.Atendimento
{
    public partial class Frmmenutemporario2 : Form
    {
        public Frmmenutemporario2()
        {
            InitializeComponent();
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void popBaixaRendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmcadpopbaixarenda _frmbaixarenda = new Song.Visualizacao.Atendimento.frmcadpopbaixarenda();
            _frmbaixarenda.Show();
        }

        private void popDeRuaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmcadpoprua _frmpoprua = new Song.Visualizacao.Atendimento.frmcadpoprua();
            _frmpoprua.Show();
        }

        private void entidadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmcadentidade _frmEntidades = new Song.Visualizacao.Atendimento.frmcadentidade();
            _frmEntidades.Show();
        }

        private void agendaMedicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmprontagenda _frmAgenda = new Song.Visualizacao.Atendimento.frmprontagenda();
            _frmAgenda.Show();
        }

        private void inserirAtendimentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmInsereAtendimento _frmInsereAtend = new Song.Visualizacao.Atendimento.frmInsereAtendimento();
            _frmInsereAtend.Show();
        }

        private void solicitarMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmsolicitarmedicamentos _frmSolMedicamentos = new Song.Visualizacao.Atendimento.frmsolicitarmedicamentos();
            _frmSolMedicamentos.Show();
        }

        private void darBaixaMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmdarbaixamedicamento _frmBaixaMedicamentos = new Song.Visualizacao.Atendimento.frmdarbaixamedicamento();
            _frmBaixaMedicamentos.Show();
        }

        private void projetoCompartilharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmAtendimentoPjc _frmAtendimentopjc = new Song.Visualizacao.Atendimento.frmAtendimentoPjc();
            _frmAtendimentopjc.Show();
        }

        private void relatóriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmrelatoriospjc _frmRelatorio = new Song.Visualizacao.Atendimento.frmrelatoriospjc();
            _frmRelatorio.Show();
        }

        private void atendimentoDiarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Song.Visualizacao.Atendimento.frmaddatendimentodiario _frmAtendimentoDia = new Song.Visualizacao.Atendimento.frmaddatendimentodiario();
            _frmAtendimentoDia.Show();
        }

        private void abrigamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultarCadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }
    }
}
